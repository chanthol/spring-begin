package edu.rupp.student.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"edu.rupp.student"})
public class MainConfig {
	
}
