package edu.rupp.student.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.rupp.student.dao.StudentDao;
import edu.rupp.student.dto.StudentDto;

@Service
public class StudentService {

	@Autowired()
	private StudentDao dao;

	public boolean add(StudentDto dto) {
		return dao.add(dto);
	}

	public StudentDto get(String id) {
		return dao.get(id);
	}

	public boolean update(StudentDto dto) {
		return dao.update(dto);
	}

	public boolean remove(String id) {
		return dao.remove(id);
	}
}
